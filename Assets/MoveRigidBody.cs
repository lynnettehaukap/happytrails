﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRigidBody : MonoBehaviour {
    public Vector3 teleportPoint;
    public Rigidbody rb;
    void OnMouseDown()
    {
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
        rb.AddForce(transform.up * 500);
    }

}
