﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using System;
using System.Threading;

public class SnailButton : MonoBehaviour
{

    public static GameObject obj1;
    public static GameObject obj;
    float timeDelay = 0;
    float desiredScale = 0.2F;
    public Button yourButton;


    public void Wrapper()
    {
        //Button btn = yourButton.GetComponent<Button>();
        //btn.onClick.AddListener(TaskOnClick);
        UnityEngine.Debug.Log("Wrapper", gameObject);
        StartCoroutine(SpawnWait());
       
    }

    public IEnumerator SpawnWait()
    {
        UnityEngine.Debug.Log("SpawniWait", gameObject);
        string s;
        float lifetime = 3.0F;
        float spawnWait = 3;

        yield return new WaitForSeconds(spawnWait);
      
            Destroy(GameObject.Find(GlobalGameData.enemy), lifetime);


       
        yield return new WaitForSeconds(spawnWait);
        {
            desiredScale = 0.4F;
            // s = GlobalGameData.enemy1;
            s = "chess";
            Sprite aSprite1 = Resources.Load(s, typeof(Sprite)) as Sprite;
            obj1 = new GameObject(s);
            SpriteRenderer renderer3 = obj1.AddComponent<SpriteRenderer>();
            obj1.transform.position = new Vector2(6, 2);
            obj1.transform.localScale = new Vector2(desiredScale, desiredScale);
            renderer3.sprite = aSprite1;
        }

       
        yield return new WaitForSeconds(spawnWait);
        {
            Destroy(GameObject.Find(GlobalGameData.enemy1));
            desiredScale = 0.6F;
            //s = GlobalGameData.enemy2;
            s = "bun_1";
            Sprite aSprite2 = Resources.Load(s, typeof(Sprite)) as Sprite;
            obj = new GameObject(GlobalGameData.enemy1);
            SpriteRenderer renderer2 = obj.AddComponent<SpriteRenderer>();
            obj.transform.position = new Vector2(3, 1);
            obj.transform.localScale = new Vector2(desiredScale, desiredScale);
            //transform.Rotate(Vector3.forward * -90);
            renderer2.sprite = aSprite2;
        }

        spawnWait += 3;
        yield return new WaitForSeconds(spawnWait);
        {
            SwitchScene();
        }
        
    }

    void SwitchScene()
    {
        int nextScene = 1;//map

        if (GlobalGameData.dangerEncounter)
        {
            //flip to rand chosen danger encounter scene
            SceneManager.LoadScene(GlobalGameData.dangerScene);
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(GlobalGameData.dangerScene));
        }
        else
        {
            SceneManager.LoadScene(nextScene);
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(nextScene));
        }
    }
}
