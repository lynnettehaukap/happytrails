﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//TODO Add scene changing according to rand danger function outcome(write randDanger())//default scene change goes to mapScene

public class ChangeScene : MonoBehaviour {
    private void Awake()
    {
        GlobalGameData.forageItems.Add("bun_1");
        GlobalGameData.forageItems.Add("sun_2");
        GlobalGameData.forageItems.Add("chess");
    }

    public void ChangeAScene (int level) {
   
        SceneManager.LoadScene(level);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(level) );//can use .GetSceneByBuildIndex or .GetSceneAt

        //call randomize function for snail and antagonist sprites for scene
        switch (level)
        {
            case 2:
                GlobalGameData.scene = 2;
                RandomizeForage();
                GlobalGameData.dangerScene = 3;
                break;
            //case 2:
                //GlobalGameData.scene = 2;
               // RandomizeRest();
               // break;
            case 3:
                GlobalGameData.scene = 3;
                RandomizeFight();
                break;
            case 4:
                GlobalGameData.scene = 4;
                RandomizeExplore();
       

                break;
            case 5:
                //save game and exit
                break;
            default:
                break;
        }

        //randomize output for activity
        //display corresponding scene
        
		
	}


    private void RandomizeForage()
    {
        int rand = Random.Range(0,2);
        //Sprite snail;
        //Sprite forageItem;
        //random generation for snail forage enum
        //random generator for forageItem 

        int item = rand;
        GlobalGameData.snail = "snail";
        GlobalGameData.currAction = 0;
        GlobalGameData.forageItem = GlobalGameData.forageItems[item];
        GlobalGameData.enemy = "Enemy";
        GlobalGameData.enemy1 = "Enemy1";
        GlobalGameData.enemy2 = "Enemy2";

        //TODO add functionality event handling and thus mini story for double click, or swipe, etc
        /*
        //add snail to scene
        GameObject obj = new GameObject("owl");
        SpriteRenderer renderer = obj.AddComponent<SpriteRenderer>();
        string s = "owl_1";
        renderer.sprite = Resources.Load(s, typeof(Sprite)) as Sprite; 

        //add forageItem to scene
        GameObject obj1 = new GameObject("bun");
        SpriteRenderer renderer1 = obj1.AddComponent<SpriteRenderer>();
        renderer1.sprite = Resources.Load("bun_2", typeof(Sprite)) as Sprite;
        */
        //string s1 = "tester";
        //UnityEditor.SceneManagement.EditorSceneManager.SaveScene(s1);
        //UnityEditor.PrefabUtility.DisconnectPrefabInstance(this);
    }


    private void RandomizeFight()
    {

    }


    private void RandomizeRest()
    {

    }


    private void RandomizeExplore()
    {

    }



}