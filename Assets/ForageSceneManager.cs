﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//TODO
//handle collected forage items
//add character specific items for each level?
//randomize items and obstacles based on scene needed and fill randLists

public class ForageSceneManager : MonoBehaviour {
    int level = ForageSceneData.level;
    int numObstacles = GlobalGameData.level + 3;//num of obstacles scale with level
    Vector3 targetPosition;
    Vector3 lookAtTarget;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    Vector3 vec;
    bool mouseClicked = false;
    Vector3 pos;
    GameObject image;
    bool obstacleCollision = false;
    bool itemCollision = false;
    public static List<string> randForageItems = new List<string>();//random selection for forage items for a UI image
    public static List<string> randForageObstacles = new List<string>();//rand selection for obstacles for a UI image
    int direction = -1;

    // Use this for initialization
    void Start () {
        //randomize scene
        //System.Random r = new System.Random();
        //int scene = r.Next(0, 10);
        ForageSceneData.SetForageItems();
        ForageSceneData.SetForageObstacles();
        //TODO randomize items and obstacles based on scene needed and fill randLists

        switch (level)
        {
            case 0:
                //TODO level determines number of obstacles, set of items available
                //popuate local data structures for obstacles and items for display
                AddRandomizedItems(level);
                AddRandomizedObstacles(level);
                //Write function for timing for lifespan and staggered appearance of items
                //display obstacles
                break;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        //handle timing/display of items
        if (Input.GetMouseButton(0))
        {
            pos = Input.mousePosition;
            Debug.Log("Pressed left click." + pos);

        }


        if (Input.GetMouseButton(0))
        {
            mouseClicked = true;
            SetTargetPosition();
            ForageItemCollisionDetection();
            //todo create struct for adjusting boundary inside rect transform for non square obstacles
            obstacleCollision = ObstacleCollisionDetection();      
            Move();
        }
        
        DisplayScene1(level);
    }

    void DisplayScene1(int scene)
    {

    }


    void SetTargetPosition()
    {
        targetPosition = Input.mousePosition;
        Debug.Log("Hit Point " + targetPosition);
    }

    void Move()
    {
        //TODO use forage snails to get correct snail image or create seasonal snail images if more needed
        GameObject image = GameObject.Find("snail");
        SetTargetPosition();

        if (obstacleCollision)
        {
            Vector2 rebound = new Vector2();
            Debug.Log(direction + "FUCKIN COLL");

            //rebound snail away from collision object
            if (direction == 0)//rebound snail left
            {
                rebound = image.transform.position;
                rebound[0] = image.transform.position[0] + 10;
                image.transform.position = Vector2.MoveTowards(image.transform.position, rebound, 100 * Time.deltaTime);
            }
            else if (direction == 1)//rebound snail right
            {
                rebound = image.transform.position;
                rebound[0] = image.transform.position[0] - 10;
                image.transform.position = Vector2.MoveTowards(image.transform.position, rebound, 100 * Time.deltaTime);
            }
            else if (direction == 2)//rebound snail down
            {
                rebound = image.transform.position;
                rebound[1] = image.transform.position[1] + 10;
                image.transform.position = Vector2.MoveTowards(image.transform.position, rebound, 100 * Time.deltaTime);
            }
            else if (direction == 3)//rebound snail up
            {
                rebound = image.transform.position;
                rebound[1] = image.transform.position[1] - 10;
                image.transform.position = Vector2.MoveTowards(image.transform.position, rebound, 100 * Time.deltaTime);
            }
        }

        else
        {
            //move snail towards mouse click position
            if (mouseClicked)
                image.transform.position = Vector2.MoveTowards(image.transform.position, targetPosition, 100 * Time.deltaTime);
        }
        
    }

    //TODO randomize based on season/level and add to randForageItem list instead of full copy of list
    void AddRandomizedItems(int level)
    {
        for(int i = 0; i < ForageSceneData.springForageItems.Count; ++i)
        {
            randForageItems.Add(ForageSceneData.springForageItems[i]);
        }
    }
    //TODO randomize based on season/level and add to randForageObstacles list instead of full copy of list
    void AddRandomizedObstacles(int level)
    {
        for (int i = 0; i < ForageSceneData.springForageObstacles.Count; ++i)
        {
            randForageObstacles.Add(ForageSceneData.springForageObstacles[i]);
        }
    }


    //collision handling for items
    void ForageItemCollisionDetection()
    {
        Debug.Log("IN PROGRESS");
        for (int i = 0; i < randForageItems.Count; ++i)
        {
            bool isCollision = ItemCollisionDetection(randForageItems[i]);
            if (isCollision)
            {
                ForageItemCollisionResponse(randForageItems[i]);
               
            }
        }
    }

    //detect snail collision with object
    bool ObstacleCollisionDetection()
    {
      //TODO use forageData.forageSnails or create seasonal snail list if more snails needed
     

        //check for collisions with all objects on scene
        for (int i = 0; i < randForageObstacles.Count; ++i)
        {
            GameObject snail = GameObject.Find("snail");
            RectTransform trans1 = snail.GetComponent<RectTransform>();
            Vector3[] snailCorners = new Vector3[4];
            trans1.GetWorldCorners(snailCorners);
            float sWidth = trans1.rect.width;
            float sHeight = trans1.rect.height;
            //get corners of snail
            Vector3 sBottomLeft = snailCorners[0];

            string item = randForageObstacles[i];
            image = GameObject.Find(item);
            RectTransform trans = image.GetComponent<RectTransform>();
            Vector3[] corners = new Vector3[4];
            trans.GetWorldCorners(corners);
            float objWidth = trans.rect.width;
            float objHeight = trans.rect.height;
            Vector3 bottomLeft = corners[0];

            if (!(sBottomLeft[0] < bottomLeft[0] + objWidth))
            {
                direction = 0;
                return false;
            }
            if (!(sBottomLeft[0] + sWidth > bottomLeft[0]))
            {
                direction = 1;
                return false;
            }
            if (!(sBottomLeft[1] < bottomLeft[1] + objHeight))
            {
                direction = 2;
                return false;
            }
            if (!(sBottomLeft[1] + sHeight > bottomLeft[1]))
            {
                direction = 3;
                return false;
            }
            
        }
        return true;
    }

    //detect snail collision with object
    bool ItemCollisionDetection(string item)
    {
        image = GameObject.Find(item);
        if (image != null)
        {
            RectTransform trans = image.GetComponent<RectTransform>();
            Vector3[] corners = new Vector3[4];
            trans.GetWorldCorners(corners);
            float objWidth = trans.rect.width;
            float objHeight = trans.rect.height;

            //get corners of forage object 
            Vector3 bottomLeft = corners[0];
            GameObject snail = GameObject.Find("snail");
            RectTransform trans1 = snail.GetComponent<RectTransform>();
            Vector3[] snailCorners = new Vector3[4];
            trans1.GetWorldCorners(snailCorners);
            float sWidth = trans1.rect.width;
            float sHeight = trans1.rect.height;

            //get corners of snail
            Vector3 sBottomLeft = snailCorners[0];

            if (sBottomLeft[0] < bottomLeft[0] + objWidth &&
                sBottomLeft[0] + sWidth > bottomLeft[0] &&
                sBottomLeft[1] < bottomLeft[1] + objHeight &&
                sBottomLeft[1] + sHeight > objHeight)
                return true;
        }
        return false;

    }

    void ForageItemCollisionResponse(string item)
    {
        //TODO
        //add item to collected list
        GameObject obj = GameObject.Find(item);
        Destroy( obj , 3);
        ForageSceneData.springForageItems.Remove(item);
        Debug.Log(item + "collected");
    }
}



