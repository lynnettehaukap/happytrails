﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailForageController : MonoBehaviour {
    Vector3 targetPosition;
    Vector3 lookAtTarget;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    Vector3 vec;

    // Use this for initialization
    void Start () {
    
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            SetTargetPosition();
        }
        Move();
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
     
        vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);


        /*
        //test for 2d colliders
        Vector3 dir = gameObject.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //endtest
           */
        //working code for 3d colliders
        //Debug.Log("Snail Controller");
        if (Physics.Raycast(ray, out hit, 1000))
        {

            //working move code
            targetPosition = hit.point;
            lookAtTarget = new Vector3(vec.x - transform.position.x, transform.position.y, 1);
            playerRot = Quaternion.LookRotation(lookAtTarget);
           
        }
     
    }

    void Move()
    {
        //Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
       // rb.velocity = vec * speed;
        //rb.AddForce(transform.up, ForceMode.Acceleration);
        //working code
        transform.position = Vector2.MoveTowards(transform.position, vec, speed * Time.deltaTime);
    }
}
 