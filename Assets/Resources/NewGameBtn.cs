﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGameBtn : MonoBehaviour {

	
    public void WrapperNewGameBtn(int scene)
    {
        StartCoroutine(LoadMapScene(scene));
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(scene));
        UnityEngine.Debug.Log("Load Game Wrapper", gameObject);
    }

    // TODO write coroutine to populate map 
    public IEnumerator LoadMapScene(int scene) {
        int spawnWait = 1;
        SceneManager.LoadScene(scene);
        yield return new WaitForSeconds(spawnWait);
    }
}
