﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public static class GlobalGameData {
  
  
    public enum Action
    {
        Start,
        Map,
        Forage,
        Fight,
        Rest,
        Explore,
        None
    }

   // public enum ForageItems
   // {
       // chess,
       // bun_1,
       // sun_2,
   // }

    public enum DangerSceneEnum
    {
        Forage = 3,
        None = 1,
        Tester = 4,
        Start = 0
    }

    public static int scene = 1;//main menu
    public static int level = 0;//beginning
    private static float health = 1;
    private static float strength = 1;
    public static string snail = "orig";
    public static string forageItem = "none";
    public static Action currAction = Action.None;
    public static string enemy = "none";
    public static string enemy1 = "none";
    public static string enemy2 = "none";
    public static bool btnClicked = false;
    public static List<string> forageItems = new List<string>();
    public static List<string> forageSnails = new List<string>();
    public static bool dangerEncounter = false;
    public static bool randDangerSceneEnum;
    public static int dangerScene = 1;
    public static int forageSnailMax = 2;
    public static int forageItemMax = 3;
    public static int forageSnailIndex = 0;

    public struct ForageStruct
    {
        public static List<ObjectData> backGround = new List<ObjectData>();
        public static ObjectData foreGround = new ObjectData();
        public static ObjectData protagonist = new ObjectData();//todo change to list of sprites for animation?
        public static ObjectData antagonist = new ObjectData();
    }

    // public static List<string> Items = new List<string>();

    public static void SetForageSnails()
    {
        forageSnails.Add("snail");
        forageSnails.Add("snail_1");
        forageSnails.Add("snail_2");
    }

    public static void InitGame()
    {
        ForageSceneData.SetForageItems();
        Debug.Log("InitGame");
    }

}
