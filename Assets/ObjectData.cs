﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectData : MonoBehaviour {
    public string spriteName = "none";
    public int level = 0;
    public float xVal = 0;
    public float yVal = 0;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetObjectData(string name, int lev, float x, float y)
    {
        spriteName = name;
        level = lev;
        xVal = x;
        yVal = y;
    }


}
