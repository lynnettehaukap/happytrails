﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollisionDetection : MonoBehaviour {

    Vector3 targetPosition;
    Vector3 lookAtTarget;
    Quaternion playerRot;
    float rotSpeed = 5;
    float speed = 10;
    Vector3 vec;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            SetTargetPosition();
            CollisionDetection();
        }
        Move();
       // if (CollisionDetected())
        {
           // Debug.Log("MOTHA FUCKA");
        }

    }

    void SetTargetPosition()
    {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);


        /*
        //test for 2d colliders
        Vector3 dir = gameObject.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //endtest
           */
        //working code for 3d colliders
        //Debug.Log("Snail Controller");
        if (Physics.Raycast(ray, out hit, 1000))
        {

            //working move code
            targetPosition = hit.point;
            lookAtTarget = new Vector3(vec.x - transform.position.x, transform.position.y, 1);
            playerRot = Quaternion.LookRotation(lookAtTarget);

        }

    }

    void Move()
    {
        
        if (targetPosition.x < 2 )
            transform.position = Vector2.MoveTowards(targetPosition, vec,  Time.deltaTime);

        //snail move test using ui image
        GameObject image = GameObject.Find("Image");
        Vector3 target = new Vector3(1000, 1000, 0);
        if (CollisionDetection())
        {
            Debug.Log("FUCKIN COLL");

        }
        else
        {
            image.transform.position = Vector2.MoveTowards(image.transform.position, target, 100 * Time.deltaTime);
        }


    }

    bool CollisionDetection()
    {
        GameObject image = GameObject.Find("Image");
        RectTransform trans = image.GetComponent<RectTransform>();
        Vector3[] corners = new Vector3[4];
        trans.GetWorldCorners(corners);
        float objWidth = trans.rect.width;
        float objHeight = trans.rect.height;

        //get corners of forage object 
        Vector3 bottomLeft = corners[0];
        GameObject snail = GameObject.Find("snailie");
        RectTransform trans1 = snail.GetComponent<RectTransform>();
        Vector3[] snailCorners = new Vector3[4];
        trans1.GetWorldCorners(snailCorners);
        float sWidth = trans1.rect.width;
        float sHeight = trans1.rect.height;

        //get corners of snail
        Vector3 sBottomLeft = snailCorners[0];
        Debug.Log("poops" + image.transform.position);
        Vector3 target = new Vector3(1000, 1000, 0);
        if (sBottomLeft[0] < bottomLeft[0] + objWidth &&
            sBottomLeft[0] + sWidth > bottomLeft[0] &&
            sBottomLeft[1] < bottomLeft[1] + objHeight &&
            sBottomLeft[1] + sHeight > objHeight)
            return true;

        return false;
    }
}
