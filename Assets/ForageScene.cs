﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;


public class ForageScene : MonoBehaviour {
    public Transform dirtClod;
    float timeDelay = 0;
    public GameObject buttonPrefab;
    public GameObject buttonPrefab1;
    public GameObject obj;
    public GameObject forageItem;
    public GameObject snailTest;
    SpriteRenderer renderer1;
    SpriteRenderer renderer2;
    Sprite aSprite1;
    float scale = 0.10f;
    public Collider coll;
    Animator anim;
    int scene;
    public Transform target;
    Camera cam;
    ForageStruct myStruct;
    float desiredScale = 0.999F;
    RaycastHit hit;
    int enemyClickTotal = 0;

    //The start and finish positions for the interpolation
    private Vector3 startPosition;
    private Vector3 endPosition;  
   
    //private float travelTime = 0.5f;
  
    public float fracJourney;
    public Vector3 pos;
    public bool mouseClicked = false;
    float timeToReachTarget = 1;
    float time = 0;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    float shipX = 0;
    float shipY = 0;
    float easingAmount;
    float xDistance;
    float yDistance;
    double distance;
    Vector3 coord;

    public void MakeButton()
    {
        string s = "bun_1";
        // UnityEngine.UI.Button btn = GameObject.Find("myButt").GetComponent<UnityEngine.UI.Button>();//TODO GET WORKING CORRECTLY
        Sprite mySprite = Resources.Load(s, typeof(Sprite)) as Sprite;
        GameObject button = (GameObject)Instantiate(buttonPrefab);
        var panel = GameObject.Find("Canvas");
        button.transform.position = panel.transform.position;
        button.GetComponent<Image>().sprite = mySprite;
        // button.GetComponent<Text>().text = "my super button text";
        button.GetComponent<RectTransform>().SetParent(panel.transform);
        button.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 40);
        button.layer = 5;
        Button myButton = button.GetComponent<Button>();
        myButton.onClick.AddListener(() => HandleClick());

    }

    public void MakeSprite(ForageStruct myStruct)
    {
        /*
                GameObject obj1 = new GameObject(myStruct.foreGround.spriteName);
                SpriteRenderer renderer1 = obj1.AddComponent<SpriteRenderer>();
                obj1.transform.position = new Vector2(-1, -4);
                obj1.transform.localScale = new Vector2(1.5F, 0.25F);
                renderer1.sprite = Resources.Load(myStruct.foreGround.spriteName, typeof(Sprite)) as Sprite;


                     string s = myStruct.foreGround.spriteName;
                     // UnityEngine.UI.Button btn = GameObject.Find("myButt").GetComponent<UnityEngine.UI.Button>();//TODO GET WORKING CORRECTLY
                     Sprite mySprite = Resources.Load(s, typeof(Sprite)) as Sprite;
                     GameObject button1 = (GameObject)Instantiate(buttonPrefab1);
                     var panel = GameObject.Find("Canvas");
                     button1.transform.SetParent(panel.transform);
                    // button1.transform.position = panel.transform.position;
                     button1.GetComponent<Image>().sprite = mySprite;
                     // button.GetComponent<Text>().text = "my super button text";//for text instead of img
                    // button1.GetComponent<RectTransform>().SetParent(panel.transform);//panel.transform
                     //button1.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, myStruct.foreGround.xVal, myStruct.foreGround.yVal);
                     button1.layer = 5;
                     button1.transform.localPosition = new Vector3(-2, -2, 0);
                    // button1.transform.localRotation = 0.0F;
                     Button myButton = button1.GetComponent<Button>();
                     myButton.onClick.AddListener(() => HandleClick());

                     /////////////////////
                     GameObject obj1;

                      float desiredScale = 0.2F;
                      string s1 = myStruct.foreGround.spriteName;
                      Sprite aSprite1 = Resources.Load(s1, typeof(Sprite)) as Sprite;
                      //add new sprite to scene within a GameObject
                      obj1 = new GameObject("self");
                      SpriteRenderer renderer1 = obj1.AddComponent<SpriteRenderer>();
                      obj1.transform.position = new Vector2(myStruct.foreGround.xVal, myStruct.foreGround.yVal);
                     // obj1.transform.localScale = new Vector2(desiredScale, desiredScale);
                      renderer1.sprite = aSprite1;
                      */
        /*
       string s1 = myStruct.foreGround.spriteName;
       // UnityEngine.UI.Button btn = GameObject.Find("myButt").GetComponent<UnityEngine.UI.Button>();//TODO GET WORKING CORRECTLY
       Sprite mySprite1 = Resources.Load(s1, typeof(Sprite)) as Sprite;
       GameObject button1 = (GameObject)Instantiate(buttonPrefab1);
       var panel = GameObject.Find("Canvas");
       button1.transform.position = panel.transform.position;
       button1.GetComponent<Image>().sprite = mySprite1;

       button1.GetComponent<RectTransform>().SetParent(panel.transform);
       button1.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, myStruct.foreGround.xVal, myStruct.foreGround.yVal);
       button1.layer = 5;
       Button myButton1 = button1.GetComponent<Button>();
       myButton1.onClick.AddListener(() => HandleClickForeground());
       */
    }

    // Use this for initialization
    void Start() {
      
        scene = 1;
        //rand scene choice?
        // myStruct = new ForageStruct();
       // myStruct = SetScene(scene);
        MakeSprite(myStruct);

        //set scene in GlobalGame Data
        //create forage class for rand scene
        //add rand snail/item
        // MakeButton();

        string s1 = "dandelion";
        aSprite1 = Resources.Load(s1, typeof(Sprite)) as Sprite;
        //add new sprite to scene within a GameObject
        forageItem = new GameObject();
        renderer1 = forageItem.AddComponent<SpriteRenderer>();
        forageItem.transform.localScale = new Vector2(.4f, .5f);
        forageItem.transform.position = new Vector2(2, -1);
        renderer1.sprite = aSprite1;
        SphereCollider sc = forageItem.AddComponent(typeof(SphereCollider)) as SphereCollider;
        Rigidbody trinketItemRigidBody = forageItem.AddComponent<Rigidbody>();
        trinketItemRigidBody.isKinematic = true;
        trinketItemRigidBody.useGravity = false;
        //forageItem.AddComponent<MouseHandler>();
        forageItem.layer = 0;
        forageItem.GetComponent<SpriteRenderer>().sortingOrder = 2;
        coll = forageItem.GetComponent<Collider>();

        obj = GameObject.Find("snail");
        obj.AddComponent<MouseHandler>();

       // GameObject targetTransform = GameObject.Find("dirtClod");
        //target = targetTransform.transform;
        /*
        //test hide-pop up game play
        string s = "snail_1";
        Sprite aSprite2 = Resources.Load(s, typeof(Sprite)) as Sprite;
        //add new sprite to scene within a GameObject
        snailTest = new GameObject();
        renderer2 = snailTest.AddComponent<SpriteRenderer>();
        snailTest.transform.localScale = new Vector2(scale, scale);
        snailTest.transform.position = new Vector2(5, 4);
        renderer2.sprite = aSprite2;
        */
        //snailTest.transform.localScale = new Vector2(desiredScale, desiredScale);

        startPosition = obj.transform.position;
        startTime = Time.time;
   
        StartCoroutine(SpawnWait());

    }

    // Update is called once per frame
    private void Update()
    {

        timeDelay = 0;
        timeDelay += Time.deltaTime;

    

        // yield return new WaitForSeconds(shortWait);
        {

            //string s = myStruct.foreGround.spriteName;
            
                //obj.transform.Translate(Vector3.up * timeDelay/2);
                //obj.transform.Translate(Vector3.right * timeDelay/2, Space.World);
            ////////
            //obj.transform.Translate(0, 0, Time.deltaTime);
            //obj.transform.Translate(0, Time.deltaTime, 0, Space.World);
            // obj.transform.position = new Vector2(-6, -3);
                //obj.transform.localScale = new Vector2(desiredScale, desiredScale);
            //forageItem.transform.localScale = new Vector2(scale, scale);
            //scale forageItem size
            if(scale < 0.5f)
            {
                scale += 0.01f;
            }

            //scale snail size
            if (desiredScale > 0.4F)
                desiredScale -= 0.003F;
        }
    }

    public IEnumerator SpawnWait()
    {

        UnityEngine.Debug.Log("SpawnWait", gameObject);
        string s;
        float lifetime = 3.0F;
        float spawnWait = 3;
        float shortWait = 0.5f;
        float medWait = 1.5f;
        float moveSpeed = 0.20f;
        float timeDelay = 0;
        int min = -8;
        int max = 8;
        float xRand = UnityEngine.Random.Range(min, max);
        float yRand = UnityEngine.Random.Range(min, max);
        timeDelay += Time.deltaTime;
        scale = 0.1f;

        for (float i = 0; i <= 1;)
        {
           // obj.transform.position = Vector3.Lerp(obj.transform.position, hit.point, i);
            i += .0001f;
        }
        // yield return new WaitForSeconds(spawnWait);

        
      
       // Debug.Log(hit.point);
        yield return new WaitForSeconds(shortWait);
        /*
        {

            //string s = myStruct.foreGround.spriteName;
           // obj = GameObject.Find("snailsRevised");

            //obj.transform.Translate(Vector3.up * moveSpeed);
            //obj.transform.Translate(Vector3.right * moveSpeed, Space.World);
           
            
            //obj.transform.Translate(0, 0, Time.deltaTime);
            //obj.transform.Translate(0, Time.deltaTime, 0, Space.World);
            // obj.transform.position = new Vector2(-6, -3);

          

            //obj.transform.localScale = new Vector2(desiredScale, desiredScale);
            //if (timeDelay >= 1)
            {
                if (desiredScale > 0.4F)
                    desiredScale -= 0.003F;
            }
        }
        yield return new WaitForSeconds(shortWait);
        {
            forageItem.transform.position = new Vector2(3, 1);
        }
        yield return new WaitForSeconds(shortWait);
        forageItem.transform.position = new Vector2(2, -1);
        //forageItem.transform.Translate(Vector3.up * Time.deltaTime);
        //forageItem.transform.Translate(Vector3.right * Time.deltaTime, Space.World);


        
         yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(3, 1);
                //forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        yield return new WaitForSeconds(shortWait);
        forageItem.transform.position = new Vector2(2, -1);

        yield return new WaitForSeconds(medWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(3, 1);
                //forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        yield return new WaitForSeconds(shortWait);
        forageItem.transform.position = new Vector2(2, -1);

        //short wait double pop up
        yield return new WaitForSeconds(shortWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(3, 1);
                //forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        yield return new WaitForSeconds(shortWait);
        forageItem.transform.position = new Vector2(2, -1);

        yield return new WaitForSeconds(shortWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(3, 1);
                //forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        yield return new WaitForSeconds(shortWait);
        forageItem.transform.position = new Vector2(2, -1);

        /*
        xRand = UnityEngine.Random.Range(min, max);
        yRand = UnityEngine.Random.Range(min, max);
        spawnWait = 0.25f;
        yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.1f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(xRand, yRand);
                forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        xRand = UnityEngine.Random.Range(min, max);
        yRand = UnityEngine.Random.Range(min, max);
        yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(xRand, yRand);
                forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        xRand = UnityEngine.Random.Range(min, max);
        yRand = UnityEngine.Random.Range(min, max);
        yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.1f;
            if (forageItem != null)
            {

                forageItem.transform.position = new Vector2(xRand, yRand);
                forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        xRand = UnityEngine.Random.Range(min, max);
        yRand = UnityEngine.Random.Range(min, max);
        yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.0f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(xRand, yRand);
                forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        xRand = UnityEngine.Random.Range(min, max);
        yRand = UnityEngine.Random.Range(min, max);
        yield return new WaitForSeconds(spawnWait);
        {
            scale = 0.1f;
            if (forageItem != null)
            {
                forageItem.transform.position = new Vector2(xRand, yRand);
                forageItem.transform.localScale = new Vector2(scale, scale);
            }
        }
        */
    }



    public void HandleClick()
    {
        Debug.Log("Button clicked SUCESS");
    }

    public void HandleClickForeground()
    {
        Debug.Log("GRASS");
    }

    void Destroy()
    {
        //myselfButton.onClick.RemoveListener(() => actionToMaterial(index));
    }


    public struct ForageStruct
    {
        public List<ObjectData> backGround;
        public ObjectData foreGround;
        public ObjectData trinket;
        public ObjectData antagonist;
    }

    public ForageStruct SetScene(int scene)
    {
        ForageStruct myStruct = new ForageStruct();
        string name;
        int level;
        float x;
        float y;

        if (scene == 1)
        {
            //spring
            name = "grass";
            level = 3;
            x = 200;
            y = 200;

            //background sprites

            //foreground
            myStruct.foreGround = new ObjectData();
            myStruct.foreGround.SetObjectData(name, level, x, y);
        }
        return myStruct;
    }

    /*
    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 100), "I am a button"))
            print("You clicked the button!");

    }
    */
}
