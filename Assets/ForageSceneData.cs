﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//TODO
//multiple forage scenes
//by forage scene
//forage items
//obstacle items

public class ForageSceneData : MonoBehaviour {

    public static int level = 0;
    public static List<string> forageSnails = new List<string>();
    //seasonal items and obstacles
    public static List<string> springForageItems = new List<string>();//name of image to create a UI image for item
    public static List<string> springForageObstacles = new List<string>();//name of image to create a UI image for obstacle
    public static List<string> summerForageItems = new List<string>();//name of image to create a UI image for item
    public static List<string> summerForageObstacles = new List<string>();//name of image to create a UI image for obstacle
    public static List<string> fallForageItems = new List<string>();//name of image to create a UI image for item
    public static List<string> fallForageObstacles = new List<string>();//name of image to create a UI image for obstacle
    public static List<string> winterForageItems = new List<string>();//name of image to create a UI image for item
    public static List<string> winterForageObstacles = new List<string>();//name of image to create a UI image for obstacle

    public static void SetForageItems()
    {
        //springForageItems.Add("star");
        springForageItems.Add("square");
        //springForageItems.Add("circle");
    }
    
    public static void SetForageObstacles()
    {
        springForageObstacles.Add("grass");
        //springForageObstacles.Add("treeCopse");
    }

    public static void SetForageSnails()
    {
        forageSnails.Add("snail");
    }

    public struct ForageStruct
    {
        public List<ObjectData> backGround;
        public ObjectData foreGround;
        public ObjectData protagonist;
        public ObjectData antagonist;
    }

    public ForageStruct SetScene(int scene)
    {
        ForageStruct myStruct = new ForageStruct();
        string name;
        int level;
        int x;
        int y;

        if(scene == 1)
        {
            //spring
            name = "flowers";
            level = 3;
            x = 10;
            y = 10;

            //background sprites

            //foreground
            myStruct.foreGround = new ObjectData();
            myStruct.foreGround.SetObjectData(name, level, x, y);
        }
        return myStruct;
    }
    private void Start()
    {
        
    }


    // Update is called once per frame
    void Update () {
		
	}
}
