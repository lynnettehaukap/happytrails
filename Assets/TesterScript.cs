﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public  class TesterScript : MonoBehaviour {
     public static GameObject obj1;
     public static GameObject obj;
     float timeDelay;
     float desiredScale = 0.2F;
    Animation anim;
    GameObject foo;
    GameObject foo2;
    public float animSpeed = 1f;
 

    // Use this for initialization
    void Start () {
       
        foo = GameObject.Instantiate((GameObject)Resources.Load("yoMomma"));
        foo.transform.position = new Vector2(-6, -3);

        foo2 = GameObject.Instantiate((GameObject)Resources.Load("myProtagonist"));
        foo2.transform.position = new Vector2(1, 1);
        



        //foo.transform.localScale = new Vector2(1.5F, 0.25F);

        // foo = GameObject.Instantiate((GameObject)Resources.Load("yoMomma"));

        //obj.AddComponent<Animation>();



        //Debug.Log("You have clicked the button!");

        string s1 = GlobalGameData.forageItem;
        Sprite aSprite1 = Resources.Load(s1, typeof(Sprite)) as Sprite;
        //add new sprite to scene within a GameObject
         obj1 = new GameObject("self");
        SpriteRenderer renderer1 = obj1.AddComponent<SpriteRenderer>();
        obj1.transform.position = new Vector2(-6, -3);
        obj1.transform.localScale = new Vector2(desiredScale, desiredScale);
        renderer1.sprite = aSprite1;

        desiredScale = 0.2F;
        string s = GlobalGameData.enemy;
        Sprite aSprite = Resources.Load(s, typeof(Sprite)) as Sprite;
        //add new sprite to scene within a GameObject
        obj = new GameObject("Enemy");
        SpriteRenderer renderer = obj.AddComponent<SpriteRenderer>();
        obj.transform.position = new Vector2(7, 3);
        obj.transform.localScale = new Vector2(desiredScale, desiredScale);
        renderer.sprite = aSprite;
        
    }
    
    // Update is called once per frame
    void Update() {

        float moveSpeed =0.20f;
        timeDelay = 0;
        timeDelay += Time.deltaTime;
        //string s;

        foo.transform.Translate(Vector3.up * Time.deltaTime);
        foo.transform.Translate(Vector3.right * Time.deltaTime, Space.World);
        foo.GetComponent<Animator>().speed = 0.25f;//change animation speed

        foo2.transform.Translate(Vector3.up * Time.deltaTime);
        foo2.transform.Translate(Vector3.right * Time.deltaTime, Space.World);
        foo2.transform.Translate(Vector3.up * moveSpeed * timeDelay);//change animation movement in scene speed

        /*
            if (GlobalGameData.btnClicked)
            {
                //if (timeDelay >= 3)
                {
                    //this.gameObject.GetComponent<SpriteRenderer>().sprite = spSnail;
                    Destroy(obj);
                }

                //if (timeDelay >= 4)
                {
                    desiredScale = 0.4F;
                    s = GlobalGameData.enemy1;
                    //obj = GameObject.Find("enemy");
                    Sprite aSprite1 = Resources.Load(s, typeof(Sprite)) as Sprite;

                    obj = new GameObject("enemy1");
                    SpriteRenderer renderer1 = obj.AddComponent<SpriteRenderer>();
                    obj.transform.position = new Vector2(6, 2);
                    obj.transform.localScale = new Vector2(desiredScale, desiredScale);
                    renderer1.sprite = aSprite1;
                }

                //if (timeDelay >= 5)
                {
                    Destroy(obj);
                    desiredScale = 0.6F;
                    s = GlobalGameData.enemy2;
                    Sprite aSprite2 = Resources.Load(s, typeof(Sprite)) as Sprite;
                    obj = new GameObject("enemy2");
                    SpriteRenderer renderer2 = obj.AddComponent<SpriteRenderer>();
                    obj.transform.position = new Vector2(3, 1);
                    obj.transform.localScale = new Vector2(desiredScale, desiredScale);
                    //transform.Rotate(Vector3.forward * -90);
                    renderer2.sprite = aSprite2;
                }
            }
            */

    }

}
